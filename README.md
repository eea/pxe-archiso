# ArchIso PXE EEA

Ce dépôt contient une configuration d'une image ArchLinux.

Lors de la génération de l'image, un chroot ArchLinux sera créé dans
`root.x86_64` pour avoir les bonnes dépendances pour créer l'image.

## Configuration du serveur d'image

### Configuration du serveur NBD

Installer `nbd-server` et mettre dans `/etc/nbd-server/conf.d/arch.conf` :

```
[archiso]
    exportname = /srv/archiso.iso
    copyonwrite = false
```

Après redémarrer le service `nbd-server`.

Il faudra mettre l'iso générée de `root.x86_64/out/` vers `/srv/archiso.iso`.

### Configuration d'un serveur HTTP

Il faut servir le contenu de `http/` sur un serveur HTTP pointé dans
la configuration de grub.

### Configuration d'un serveur TFTP

Il faut servir le contenu de `tftp/` sur un serveur TFTP pointé
par le DHCP ou un autre serveur PXE.

Installer `tftpd-hpa` et placer les fichiers générés de `tftp/` vers `/srv/tftp/`.

## FAQ

### Tester l'image

Qemu permet de rapidement tester l'image construite en la bootant en PXE
de la manière suivante.

```
sudo qemu-system-x86_64 -enable-kvm -m 2048 -boot n -vga qxl \
        -machine q35 -device intel-iommu -cpu host -smp 2 -device virtio-balloon \
        -option-rom /usr/share/qemu/pxe-rtl8139.rom \
        -device e1000,netdev=n \
        -netdev user,id=n,net=10.0.0.0/24,dhcpstart=10.0.0.200,host=10.0.0.1,tftp=./tftp,bootfile=i386-pc/core.0
```

### Chaîner à partir d'un autre PXE

Pour chainer sur un autre serveur à partir d'un PXELINUX, vous pouvez exécuter,

```
KERNEL pxechn.c32 185.230.78.191::i386-pc/core.0
```
