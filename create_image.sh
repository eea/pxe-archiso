#!/bin/bash
# Written by Alexandre Iooss <erdnaxe@crans.org>

bootstrap_image_url=http://archlinux.de-labrusse.fr/iso/2020.01.01/archlinux-bootstrap-2020.01.01-x86_64.tar.gz

# Download and create archlinux chroot environment in which we can build archiso
mkdir -p .cache
wget "$bootstrap_image_url" -c -O .cache/bootstrap.tar.gz
sudo tar xzf .cache/bootstrap.tar.gz
sudo mount --bind root.x86_64 root.x86_64
sudo root.x86_64/bin/arch-chroot root.x86_64 pacman-key --init
sudo root.x86_64/bin/arch-chroot root.x86_64 pacman-key --populate archlinux
sudo sed -i "s/#Server/Server/g" root.x86_64/etc/pacman.d/mirrorlist
sudo root.x86_64/bin/arch-chroot root.x86_64 pacman -Sy squashfs-tools btrfs-progs dosfstools xorriso lynx git make arch-install-scripts sed file --needed --noconfirm
sudo umount root.x86_64

# Download archiso scripts
git clone https://git.archlinux.org/archiso.git .cache/archiso_git
sudo make -C .cache/archiso_git install DESTDIR=`pwd`/root.x86_64

# Customize archiso
cat conf/custom_packages | sudo tee -a root.x86_64/usr/share/archiso/configs/releng/packages.x86_64
sudo cp conf/pacman.conf root.x86_64/usr/share/archiso/configs/releng/pacman.conf
sudo cp conf/customize_airootfs.sh root.x86_64/usr/share/archiso/configs/releng/airootfs/root/customize_airootfs.sh
sudo rm root.x86_64/usr/share/archiso/configs/releng/airootfs/root/install.txt
sudo cp conf/locale.conf root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/locale.conf
sudo cp conf/vconsole.conf root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/vconsole.conf
sudo mkdir -p root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/dconf/db/local.d root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/dconf/profile
sudo cp conf/dconf-settings root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/dconf/db/local.d/
sudo cp conf/dconf-profile root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/dconf/profile/user
sudo rm root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/udev/rules.d/81-dhcpcd.rules
sudo mkdir -p root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/systemd/resolved.conf.d
sudo cp conf/dns_servers.conf root.x86_64/usr/share/archiso/configs/releng/airootfs/etc/systemd/resolved.conf.d/dns_servers.conf

# Build archiso in chroot
sudo root.x86_64/bin/arch-chroot root.x86_64 /usr/share/archiso/configs/releng/build.sh -v

# Create grub TFTP
rm -rfv tftp
grub-mknetdir --compress xz -d /usr/lib/grub/i386-pc --net-directory=tftp --subdir=/ --modules="http"
cp conf/grub.cfg tftp/grub.cfg

# Copy files to serve for web server
mkdir -p http
cp root.x86_64/work/iso/arch/boot/x86_64/vmlinuz http/
cp root.x86_64/work/iso/arch/boot/x86_64/archiso.img http/
cp root.x86_64/work/iso/arch/boot/amd_ucode.img http/
cp root.x86_64/work/iso/arch/boot/intel_ucode.img http/

echo "TFTP files are in `pwd`/tftp/."
echo "HTTP files are in `pwd`/http/."
echo "ISO is in `pwd`/root.x86_64/out/ and should be served by a nbd server (see README)."
