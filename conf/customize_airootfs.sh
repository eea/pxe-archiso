#!/bin/bash

set -e -u

# Generate french and english locales
sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(fr_FR\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

# Set time
ln -sf /usr/share/zoneinfo/UTC /etc/localtime

# Config pacman and systemd
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf
sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf
systemctl enable pacman-init.service choose-mirror.service

# Enable cups and gdm
systemctl enable org.cups.cupsd.service gdm.service
systemctl set-default graphical.target

# Add an user and authorize sudo
useradd -m -p "" -g users -G uucp,lock,sys,wheel -s /usr/bin/zsh user
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# Install jlink-software-and-documentation and mcuxpresso-ide
sudo -u user git clone https://aur.archlinux.org/jlink-software-and-documentation /tmp/jlink
sudo -u user git clone https://aur.archlinux.org/mcuxpresso-ide.git /tmp/mcuexpresso-ide
sudo -u user git clone https://aur.archlinux.org/eagle.git /tmp/eagle
sudo -u user git clone https://aur.archlinux.org/hplip-plugin.git /tmp/hplip-plugin
sudo -u user git clone https://aur.archlinux.org/ncurses5-compat-libs.git /tmp/ncurses5-compat-libs
sudo -u user git clone https://aur.archlinux.org/openarena.git /tmp/openarena
cd /tmp/jlink
sudo -u user makepkg -si --noconfirm
cd /tmp/mcuexpresso-ide/
sudo -u user makepkg -si --noconfirm
cd /tmp/eagle/
#sudo -u user makepkg -si --noconfirm
cd /tmp/hplip-plugin/
sudo -u user makepkg -si --noconfirm
cd /tmp/ncurses5-compat-libs/
sudo -u user gpg --recv-keys 702353E0F7E48EDB
sudo -u user makepkg -si --noconfirm

# Update dconf settings
dconf update
